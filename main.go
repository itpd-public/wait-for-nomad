package main

import (
	"fmt"
	"github.com/hashicorp/nomad/api"
	log "github.com/sirupsen/logrus"
	"os"
	"time"
)

func main() {
	var (
		jobID         = ensureEnv("JOB")
		nomadAddr     = ensureEnv("NOMAD_ADDR")
		checkInterval = 10 * time.Second
		timeoutString = ensureEnv("TIMEOUT")
	)

	timeout, err := time.ParseDuration(timeoutString)
	if err != nil {
		log.Fatalf("Can't parse timeout `%s` string: %v", timeoutString, err)
	}
	go func(timeout time.Duration) {
		time.Sleep(timeout)
		log.Fatalf("Timeout exceeded")
		os.Exit(1)
	}(timeout)

	cl, err := api.NewClient(&api.Config{
		Address: nomadAddr,
	})
	if err != nil {
		log.Fatalf("Can't create nomad API client: %v", err)
	}

	dep, err := searchForDeployment(cl, jobID)
	if err != nil {
		log.Fatalf("Can't get a deployment: %v", err)
	}
	log.Infof("Found a deployment `%s` with status `%s`", dep.ID, dep.Status)

	_, err = searchForJob(cl, jobID)
	if err != nil {
		log.Fatalf("Can't get a job: %v", err)
	}

	waitForDeploymentEnd(cl, dep, checkInterval)
}

func searchForJob(cl *api.Client, jobID string) (*api.Job, error) {
	j, _, err := cl.Jobs().Info(jobID, &api.QueryOptions{})
	if err != nil {
		return nil, fmt.Errorf("can't load a job by api: %v", err)
	}
	if j == nil {
		return nil, fmt.Errorf("job `%s` not found in the nomad", jobID)
	}
	return j, nil
}

func searchForDeployment(cl *api.Client, job string) (*api.Deployment, error) {
	deployments, _, err := cl.Deployments().List(nil)
	if err != nil {
		return nil, fmt.Errorf("can't load a deployment by api: %v", err)
	}
	for _, deployment := range deployments {
		if job == deployment.JobID {
			return deployment, nil
		}
	}
	return nil, fmt.Errorf("deployment for a job `%s` not found in the nomad", job)
}

func waitForDeploymentEnd(cl *api.Client, dep *api.Deployment, interval time.Duration) {
	for {
		updatedDep, _, err := cl.Deployments().Info(dep.ID, &api.QueryOptions{})
		if err != nil {
			log.Fatalf("Can't reload a deployment: %v", err)
		}

		switch updatedDep.Status {
		case "successful":
			log.Infof("Deployment successful")
			os.Exit(0)
		case "failed":
			log.Infof("Deployment failed")
			os.Exit(1)
		case "running":
			st := ""
			finishedGroups := 0
			for group, state := range updatedDep.TaskGroups {
				if state.PlacedAllocs == state.DesiredTotal {
					finishedGroups++
				}
				st = fmt.Sprintf("%s%s[%d/%d]\t\t", st, group, state.PlacedAllocs, state.DesiredTotal)
			}
			log.Infof("`%s` [%s]: %s", updatedDep.StatusDescription, updatedDep.Status, st)
			time.Sleep(interval)
		default:
			log.Fatalf("Unsupported deployment status `%s` (%s)", updatedDep.Status, updatedDep.StatusDescription)
		}
	}
}

func ensureEnv(name string) string {
	v := os.Getenv(name)
	if v == "" {
		log.Fatalf("env variable `%s` is empty", name)
	}
	return v
}
